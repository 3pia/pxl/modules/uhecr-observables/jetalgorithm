// JetAlgorithm Module for PXL, 17.11.09, Peter Schiffer
// This module creates RegionsOfInterest in a basiccontainer with a jet algorithm


#include "pxl/modules/Module.hh"
#include "pxl/modules/ModuleFactory.hh"
#include "pxl/astro.hh"
#include "pxl/core.hh"
#include <vector>

#include <PXLAstroTools/acceptAuger.hh>
#include "PXLAstroTools/augerexposure.hh"

namespace pxl {

class JetAlgorithmModule: public Module {

private:
//	long _Count;
	int64_t _NRuns;
	double _SizeOfRoi;
	double _cosSizeOfRoi;
	double _SeedEnergy;
	bool _AugerAcceptance;

	void initializeROI(BasicContainer* sky) {
		int numRois = 0;
		std::vector<UHECR*> cm;
		sky->getObjectsOfType<UHECR> (cm);
		for (std::vector<UHECR*>::iterator iter = cm.begin(); iter
				!= cm.end(); ++iter) {
			UHECR* cosmic = *iter;
			if (cosmic->getEnergy() > _SeedEnergy) {
				RegionOfInterest* roi = new RegionOfInterest();
				roi->setConeRadius(_SizeOfRoi);
				roi->setName("JetROI");
				roi->setGalacticDirectionVector(cosmic->getGalacticDirectionVector());
				roi->setUserRecord("initialLon", cosmic->getGalacticLongitude());
				roi->setUserRecord("initialLat", cosmic->getGalacticLatitude());
				sky->insertObject(roi);
				numRois++;
			}
		}
		std::cout << "Number of ROIs: " << numRois << std::endl;
	}

	void assignAllToJet(BasicContainer* sky) {
		std::vector<UHECR*> cm;
		sky->getObjectsOfType<UHECR> (cm);

		std::vector<RegionOfInterest*> myjets;
		sky->getObjectsOfType<RegionOfInterest> (myjets);

		for (std::vector<RegionOfInterest*>::iterator iter =
				myjets.begin(); iter != myjets.end(); ++iter) {
			RegionOfInterest* roi = *iter;
			Basic3Vector jetdirection = roi->getGalacticDirectionVector();
			for (std::vector<UHECR*>::iterator iter2 = cm.begin(); iter2
					!= cm.end(); ++iter2) {
				UHECR* event = *iter2;
				Basic3Vector eventdirection =
						event->getGalacticDirectionVector();
				double distance = jetdirection * eventdirection;
				if (distance > _cosSizeOfRoi) {
					roi->linkSoft(event, "jetlink");
				}
			}
		}
	}

	void removeAllFromJet(BasicContainer* sky) {
		std::vector<UHECR*> cm;
		sky->getObjectsOfType<UHECR> (cm);

		std::vector<RegionOfInterest*> myjets;
		sky->getObjectsOfType<RegionOfInterest> (myjets);

		for (std::vector<RegionOfInterest*>::iterator iter =
				myjets.begin(); iter != myjets.end(); ++iter) {
			RegionOfInterest* roi = *iter;
			Basic3Vector jetdirection = roi->getGalacticDirectionVector();
			for (std::vector<UHECR*>::iterator iter2 = cm.begin(); iter2
					!= cm.end(); ++iter2) {
				UHECR* event = *iter2;
				roi->unlinkSoft(event, "jetlink");
			}
		}
	}

	void getCM(BasicContainer* sky) {
		std::vector<RegionOfInterest*> myjets;
		sky->getObjectsOfType<RegionOfInterest> (myjets);

		for (std::vector<RegionOfInterest*>::iterator iter =
				myjets.begin(); iter != myjets.end(); ++iter) {

			std::vector<Serializable*> particleinroi;
			// create a pointer to the iter
			RegionOfInterest* roi = *iter;
			roi->getSoftRelations().getSoftRelatives(particleinroi, *sky);

			//remove non UHECR from vector
			std::vector<UHECR*> crinroi;
			for (std::vector<Serializable*>::const_iterator iter2 =
					particleinroi.begin(); iter2 != particleinroi.end(); ++iter2) {
				UHECR* cr = dynamic_cast<UHECR*> (*iter2);
				if (cr) {
					crinroi.push_back(cr);
				}
			}

			//calculate center of mass
			Basic3Vector centerofmass;
			double weight;
			for (std::vector<UHECR*>::const_iterator iter2 =
					crinroi.begin(); iter2 != crinroi.end(); ++iter2) {
				UHECR* mycr = dynamic_cast<UHECR*> (*iter2);
				weight = 1;
				if (_AugerAcceptance == true) {
					weight = 0;
					if (geometricExposure(mycr) > 0.01) {
						weight = 1 / geometricExposure(mycr);
					}
				}
				centerofmass += mycr->getGalacticDirectionVector()
						* mycr->getEnergy() * weight;
			}
			centerofmass.normalize();
			roi->setGalacticDirectionVector(centerofmass);
		}
	}

public:

	// initialize the super class Module as well
	JetAlgorithmModule() :
		Module() {
//		_Count = 0;
	}

	~JetAlgorithmModule() {
	}

	void initialize() throw (std::runtime_error) {
		addSink("input", "DefaultInput");
		addSource("output", "DefaultOutput");

		addOption("NRuns", "Number of iterations", int64_t(3));
		addOption("SizeOfRoi", "Cone radius of the Regions of Interest", 0.2);
		addOption("SeedEnergy", "Seed energy for Regions of Interest", 60.0);
		addOption("AugerAcceptance", "Weight CM with auger acceptance", false);
	}

	// every Module needs a unique type
	static const std::string &getStaticType() {
		static std::string type("JetAlgorithm Module");
		return type;
	}

	bool isRunnable() const {
		return false;
	}

	// static and dynamic methods are needed
	const std::string &getType() const {
		return getStaticType();
	}

	void beginJob() throw (std::runtime_error) {
		getOption("NRuns", _NRuns);
		getOption("SizeOfRoi", _SizeOfRoi);
		_cosSizeOfRoi = cos(_SizeOfRoi);
		getOption("SeedEnergy", _SeedEnergy);
		getOption("AugerAcceptance", _AugerAcceptance);
	}

	bool isExecutable() const {
		// this module does not provide events, so return false
		return false;
	}

	bool analyse(Sink* sink) throw (std::runtime_error) {
//		_Count++;

		BasicContainer *sky =
				dynamic_cast<BasicContainer *> (getSink("input")->get());
		if (sky) {
			initializeROI(sky);
			assignAllToJet(sky);
			for (int i = 0; i < _NRuns; i++) {
				getCM(sky);
				removeAllFromJet(sky);
				assignAllToJet(sky);
			}
		}
		else {
			std::cerr << "Something other than a BasicContainer passed by\n";
		}
		getSource("output")->setTargets(getSink("input")->get());
		return getSource("output")->processTargets();
	}

	void destroy() throw (std::runtime_error) {
		// only we know how to delete this module
		delete this;
	}
};

PXL_MODULE_INIT(JetAlgorithmModule)
PXL_PLUGIN_INIT

} // namespace pxl
