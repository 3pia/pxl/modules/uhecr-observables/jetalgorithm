#include <pxl/modules.hh>
#include <pxl/modules/ModuleFactory.hh>
//#include <plugins/PluginManager.hh>

#include <iostream>

#include <pxl/astro.hh>
#include <pxl/core.hh>
#include <stdlib.h>

using namespace pxl;
using namespace std;
int main()
{

// Create a new BasicContainer
BasicContainer *bc= new BasicContainer();

//add a UHECR to the bc
UHECR *cr = new UHECR();
cr->setGalacticCoordinates(0,0);
cr->setEnergy(65.0);
bc->setObject(cr);

//We need the PXL PluginManager to load the Modules by name
//This loads the Plugins from the default directories, i.e
//$HOME/.pxl-2.5/plugins
PluginManager::instance().loadPlugins();
//Load Plugins from a different path, like current directory
PluginManager::instance().loadPluginsFromDirectory(".");


//This produces a "JetAlgorithm Module" to work on BasicContainers
Module *jetAlgorithm = ModuleFactory::instance().createModule("JetAlgorithm Module");
if (jetAlgorithm== 0)
		throw std::runtime_error(std::string("JetAlgorithm not found!"));

cout << "Initialized Module: " << jetAlgorithm->getType() << endl;

// Put the basiccontainer into the input sink
jetAlgorithm->getSink("input")->set(bc);

//perform the analysis
jetAlgorithm->analyse();


}
